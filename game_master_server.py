from flask import Flask, request, jsonify
import os
import random

app = Flask(__name__)
min_number = int(os.environ.get("MIN_NUMBER", "1"))
max_number = int(os.environ.get("MAX_NUMBER", "1000"))

@app.route("/health")
def health():
    return "healthy", 200

@app.route("/hostname")
def hostname():
    return os.uname().nodename, 200

@app.route("/", methods=["POST"])
def guess_number():
    data = request.get_json()
    guess = data["guess"]
    if guess < min_number or guess > max_number:
        return jsonify({"error": "Number out of range"}), 400
    elif guess < chosen_number:
        return jsonify({"result": "higher"}), 200
    elif guess > chosen_number:
        return jsonify({"result": "lower"}), 200
    else:
        return jsonify({"result": "won", "chosen_number": chosen_number}), 200

if __name__ == "__main__":
    chosen_number = random.randint(min_number, max_number)
    app.run(debug=True, host="0.0.0.0", port=5001)
