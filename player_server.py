from flask import Flask, jsonify
import requests
import os
import random

app = Flask(__name__)
game_master_url = "http://game_master:5001"

def play():
    # generate a random number between 1 and 1000
    number = random.randint(1, 1000)

    # initialize the range
    low = 1
    high = 1000

    # keep track of the history of guesses and results
    history = []

    # make the first guess
    response = requests.post(game_master_url, json={"guess": number})
    result = response.json()["result"]
    history.append({"guess": number, "result": result})

    # loop until the correct number is found
    while result != "won":
        if result == "higher":
            low = number + 1
        elif result == "lower":
            high = number - 1

        # make the next guess
        number = random.randint(low, high)
        response = requests.post(game_master_url, json={"guess": number})
        result = response.json()["result"]
        history.append({"guess": number, "result": result})

    # return the result and history of the game
    return jsonify({"result": response.json()["result"], "history": history})

@app.route("/health")
def health():
    return "healthy", 200

@app.route("/hostname")
def hostname():
    return os.uname().nodename, 200

@app.route("/play")
def start_game():
    return play()
     
if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
